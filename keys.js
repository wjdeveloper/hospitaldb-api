module.exports = {

    database: {
        uri: process.env.DB_URI.replace('<password>', process.env.DB_PASSWORD).replace('<dbname>', process.env.DB_NAME)
    },
    jwtOpts: {
        secret: process.env.JWT_SECRET_KEY
    }
}
