const { response } = require('express');

// 3er Party Library
const bcrypt = require('bcrypt');

// Helpers
const { generarJWT } = require('../helpers');

// Models
const { Usuario } = require('../models');

const ctrl = {}


/*
    Path: /api/v1/auth/login
    Method: POST
    Desc: Inicia sesion con el correo y la contrasena
*/
ctrl.login = async (req, res=response) => {

    const { email, password } = req.body;

    try {

        const usuarioDB = await Usuario.findOne({ email });
        if ( !usuarioDB ) {
            return res.status(404).json({
                ok: false,
                msg: 'El email no existe'
            });
        }

        const passwordIsValid = bcrypt.compareSync(password, usuarioDB.password);
        if ( !passwordIsValid ) {
            return res.status(404).json({
                ok: false,
                msg: 'La password es incorrecta'
            });
        }

        // TODO: Generar JWT AQUI
        const token = await generarJWT(usuarioDB._id);

        res.json({
            ok: true,
            msg: 'Login aceptado con exito!',
            token
        });

    } catch(error) {

        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

module.exports = ctrl;
