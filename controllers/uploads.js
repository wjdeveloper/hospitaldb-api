const { v4: uuidv4 } = require('uuid');
const path = require('path');
const fs = require('fs');
const { actualizarImagen } = require('../helpers');

const ctrl = {}


ctrl.fileUpload = (req, res) => {

    const { tipo, id } = req.params;

    // Validar tipo
    const tiposValidos = ['hospitales', 'usuarios', 'medicos'];
    if ( !tiposValidos.includes(tipo) ) {
        return res.status(400).json({
            ok: false,
            msg: 'El tipo debe de ser medicos, usuarios u hospitales'
        });
    }

    // Validamos que exista un archivo
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).json({
            ok: false,
            msg: 'No hay ningun archivo!'
        });
    }

    // Procesar la imagen
    const file = req.files.imagen;
    const nombreCortado = file.name.split('.');
    const extensionArchivo = nombreCortado[ nombreCortado.length - 1 ];

    // Validar extension
    const extensionesValidas = ['jpg', 'jpeg', 'png', 'gif'];
    if ( !extensionesValidas.includes(extensionArchivo) ) {
        return res.status(400).json({
            ok: false,
            msg: 'Tipo de extension no valida, la imagen debe de tener ext png, jpeg, jpg o gif'
        });
    }

    // Generar el nombre del archivo
    const nombreArchivo = `${ uuidv4() }.${ extensionArchivo }`;

    // Path para guardar la imagen
    const path = `./uploads/${tipo}/${nombreArchivo}`;

    file.mv(path, (error) => {

        if ( error ) {
            console.log(error);
            return res.status(500).json({
                ok: false,
                msg: 'No se pudo mover la imagen'
            });
        }

        // Actualizar imagen
        actualizarImagen(tipo, id, nombreArchivo);

        res.json({
            ok: true,
            msg: 'Archivo subido',
            nombreArchivo
        });

    });

}

ctrl.obtenerImagen = (req, res) => {

    const { tipo, image } = req.params;
    const pathImg = path.resolve(__filename, `../../uploads/${ tipo }/${ image }`);

    // Imagen por defecto
    if ( fs.existsSync(pathImg) ) {
        res.sendFile(pathImg);
    } else {
        const pathImg = path.resolve(__filename, `../../uploads/no-img.jpg`);
        res.sendFile(pathImg);
    }

}

module.exports = ctrl;
