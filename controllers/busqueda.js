// Models
const { Usuario, Hospital, Medico } = require('../models');

const ctrl = {}


/*
    Path: /api/v1/busquedas/:termino
    Method: GET
    Desc: Buscar dentro de la db todos los documentos referentes al termino
*/
ctrl.todo = async (req, res) => {

    const { termino } = req.params;
    const regexp = new RegExp( termino, 'i' );

    try {

        const [ usuarios, medicos, hospitales ] = await Promise.all([

            Usuario.find({ 'nombre': regexp }),
            Medico.find({ 'nombre': regexp })
                .populate('usuario', 'nombre avatar')
                .populate('hospital', 'nombre avatar'),
            Hospital.find({ 'nombre': regexp })
                .populate('usuario', 'nombre avatar')
        ]);

        res.json({
            ok: true,
            results: {
                usuarios,
                medicos,
                hospitales
            }
        });
    } catch(error) {

        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador',
            error
        })
    }
}

/*
    Path: /api/v1/busquedas/collection/:tabla/:termino
    Method: GET
    Desc: Busqueda de documentos por collection
*/
ctrl.documentosCollection = async (req, res) => {

    const { tabla, termino } = req.params;
    const regexp = new RegExp( termino, 'i' );

    let data = [];

    switch( tabla ) {
        case 'medicos':
            data = await Medico.find({ 'nombre': regexp })
                               .populate('usuario', 'nombre avatar')
                               .populate('hospital', 'nombre avatar');
            break;
        case 'hospitales':
            data = await Hospital.find({ 'nombre': regexp })
                                 .populate('usuario', 'nombre avatar');
            break;
        case 'usuarios':
            data = await Usuario.find({ 'nombre': regexp });
            break;
        default:
            return res.status(400).json({
                ok: false,
                msg: 'La collection debe de ser medicos/hospitales/usuarios',
            });
    }

    res.json({
        ok: true,
        results: data
    })
}


module.exports = ctrl;
