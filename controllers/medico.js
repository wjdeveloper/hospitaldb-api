// Models
const { Medico } = require('../models');

const ctrl = {}

/*
    Path: /api/v1/medicos
    Method: GET
    Desc: Obtener un arreglo de Medicos creados
*/
ctrl.obtenerMedicos = async (req, res) => {

    const medicos = await Medico.find()
        .populate('usuario', 'nombre avatar')
        .populate('hospital', 'nombre avatar');

    res.json({
        ok: true,
        medicos
    })
}

/*
    Path: /api/v1/medicos
    Method: POST
    Desc: Crear un medico
*/
ctrl.crearMedico = async (req, res) => {

    const uid = req.uid;
    const { hospital } = req.params;

    const medico = await Medico({ usuario: uid, hospital, ...req.body });

    try {

        const medicoDB = await medico.save();
        res.json({
            ok: true,
            medico: medicoDB
        });

    } catch(error) {
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }

}

/*
    Path: /api/v1/medicos
    Method: PUT
    Desc: Actualiza un medico por el id
*/
ctrl.actualizarMedico = (req, res) => {

    res.json({
        ok: true,
        msg: 'Actualizar medico'
    });
}

/*
    Path: /api/v1/medicos
    Method: DELETE
    Desc: Elimina un medico por el id
*/
ctrl.eliminarMedico = (req, res) => {

    res.json({
        ok: true,
        msg: 'Eliminar medico'
    });
}

module.exports = ctrl;
