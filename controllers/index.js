module.exports = {

    hospitales: require('./hospital'),
    medicos: require('./medico'),
    usuarios: require('./usuario'),
    auth: require('./auth'),
    busqueda: require('./busqueda'),
    uploads: require('./uploads')
}
