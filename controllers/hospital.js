// Models
const { Hospital } = require('../models');

const ctrl = {}

/*
    Path: /api/v1/hospital
    Method: GET
    Desc: Obtener un arreglo de hospitales creados
*/
ctrl.obtenerHospitales = async (req, res) => {

    const hospitales = await Hospital.find()
        .populate('usuario', 'nombre avatar');

    res.json({
        ok: true,
        hospitales
    })
}

/*
    Path: /api/v1/hospital
    Method: POST
    Desc: Crear un hospital
*/
ctrl.crearHospital = async (req, res) => {

    const uid = req.uid;
    const hospital = new Hospital({ usuario: uid, ...req.body});

    try {

        const hospitalDB = await hospital.save();
        res.json({
            ok: true,
            hospital: hospitalDB
        });

    } catch(error) {
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }

}

/*
    Path: /api/v1/hospital
    Method: PUT
    Desc: Actualiza un hospital por el id
*/
ctrl.actualizarHospital = (req, res) => {

    res.json({
        ok: true,
        msg: 'Actualizar hospital'
    });
}

/*
    Path: /api/v1/hospital
    Method: DELETE
    Desc: Elimina un hospital por el id
*/
ctrl.eliminarHospital = (req, res) => {

    res.json({
        ok: true,
        msg: 'Eliminar hospital'
    });
}

module.exports = ctrl;
