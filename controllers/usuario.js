// Create by wjwebdevelopments on 30/12/2020 23:19:00

// 3er Party Library
const bcrypt = require('bcrypt');

// Helpers
const { generarJWT } = require('../helpers');

// Models
const { Usuario } = require('../models');

const ctrl = {};

/*
    Path: /api/v1/usuarios
    Method: GET
    Desc: Obtener un arreglo de usuarios registrados
*/
ctrl.obtenerUsuarios = async (req, res) => {

    const desde = parseInt(req.query['desde']) || 0;
    const limit = parseInt(req.query['limit']) || 0;


    try {

        const [ usuarios, total ] = await Promise.all([

             Usuario.find({}, 'nombre email google role avatar')
                .skip(desde)
                .limit(limit),

             Usuario.countDocuments()

        ]);

        res.json({
            status: true,
            usuarios,
            uid: req.uid,
            total
        });

    } catch(error) {

        res.status(500).json({
            ok: false,
            error
        });
    }

}

/*
    Path: /api/v1/usuarios
    Method: POST
    Desc: Crea un nuevo usuario
*/
ctrl.crearUsuario = async (req, res) => {

    const { nombre, email, password } = req.body;

    try {

        const usuarioDB = await Usuario.findOne({ email });

        if ( usuarioDB ) {

            return res.status(400).json({
                status: false,
                msg: 'El correo ya existe'
            });
        }

        const usuario = new Usuario({ nombre, email, password});

        // Password encryting
        const salt = bcrypt.genSaltSync(10);
        usuario.password = bcrypt.hashSync(password, salt);

        // Save usuario
        const nuevoUsuario = await usuario.save();

        // Generer JsonWebToken
        const token = await generarJWT(nuevoUsuario._id);

        res.json({
            status: true,
            usuario: nuevoUsuario,
            token
        });

    } catch(error) {

        res.status(500).json({
            ok: false,
            error
        })
    }

}

/*
    Path: /api/v1/usuarios
    Method: PUT
    Desc: Actualiza un usuario por su id
*/
ctrl.actualizarUsuario = async (req, res) => {

    const id = req.params.id;

    try {

        const usuarioDB = await Usuario.findById(id);

        if ( !usuarioDB ) {
            return res.status(400).json({
                ok: false,
                msg: 'El usuario no existe'
            })
        }

        const { password, google, email, ...campos } = req.body;

        if ( usuarioDB.email !== email ) {

            const existeEmail = await Usuario.findOne({ email });

            if ( existeEmail ) {
                return res.status(400).json({
                    ok: false,
                    msg: 'El email ya existe'
                });
            }
        }

        campos.email = email;
        const usuarioActualizado = await Usuario.findByIdAndUpdate(id, campos, { new: true });

        res.json({
            ok: true,
            usuario: usuarioActualizado
        });

    } catch(error) {

        res.status(500).json({
            ok: false,
            error
        });
    }


}

/*
    Path: /api/v1/usuarios
    Method: DELETE
    Desc: Elimina un usuario por su id
*/
ctrl.eliminarUsuario = async (req, res) => {


    try {

        const usuarioDB = await Usuario.findOne({ _id: req.params.id });
        if ( !usuarioDB ) {

            return res.status(404).json({
                ok: false,
                msg: 'EL usuario no existe'
            });
        }

        const usuarioEliminado = await Usuario.findByIdAndDelete({ _id: req.params.id });
        res.json({
            ok: true,
            usuarioEliminado
        })

    } catch(error) {
        res.status(500).json({
            ok: false,
            error
        });
    }

}

module.exports = ctrl;
