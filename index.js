const express = require('express');
const config = require('./server/config');



const app = config(express());

app.listen(
    app.get('port'),
    console.log(`Server running on ${app.get('env')} in port ${app.get('port')}`)
);
