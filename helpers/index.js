// Nodejs Core
const fs = require('fs');

// 3er Party Library
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

// Keys
const { jwtOpts } = require('../keys');

// Models
const { Usuario, Hospital, Medico } = require('../models');

const helpers = {}

helpers.validarCampos = (req, res, next) => {

    const errors = validationResult(req);

    if ( !errors.isEmpty() ) {
        return res.status(400).json({
            ok: false,
            errors: errors.mapped()
        });
    }

    next();
}

helpers.generarJWT = (uid) => {

    return new Promise((resolve, reject) => {

        const payload = { uid }
        jwt.sign(payload, jwtOpts.secret, { expiresIn: '12h' }, (error, token) => {

            if (error) {
                console.log(error);
                reject('No se pudo generar el JWT');
            } else {
                resolve(token);
            }
        });
    });

}

helpers.validarToken = (req, res, next) => {

    const token = req.header('x-token');
    if ( !token ) {
        return res.status(401).json({
            ok: false,
            msg: 'No hay token en la peticion'
        });
    }

    try {

        const { uid } = jwt.verify(token, jwtOpts.secret);
        req.uid = uid;
        next();

    } catch(error) {
        res.status(401).json({
            ok: false,
            msg: 'El token no es valido o ha vencido, por favor vuelve a generar un token valido'
        });
    }

}

helpers.actualizarImagen = async (tipo, id, nombreArchivo) => {

    let pathViejo = '';
    
    switch ( tipo ) {
        case 'medicos':

            // Verificar si existe el medico, si no existe retornamos falso para terminar
            const medico = await Medico.findById(id);
            if ( !medico ) {
                console.log('No es un medico por id');
                return false;
            }

            // Si existe debemos saber la ruta de la vieja imagen para eliminarla
            pathViejo = `./uploads/medicos/${ medico.avatar }`;
            if ( fs.existsSync(pathViejo) ) {
                fs.unlinkSync(pathViejo);
            }

            // Luego de eliminar el path viejo actualizamos la imagen del medico en la DB
            medico.avatar = nombreArchivo;
            await medico.save();
            return true;

            break;
        case 'hospitales':

            // Verificar si existe el medico, si no existe retornamos falso para terminar
            const hospital = await Hospital.findById(id);
            if ( !hospital ) {
                console.log('No es un hospital por id');
                return false;
            }

            // Si existe debemos saber la ruta de la vieja imagen para eliminarla
            pathViejo = `./uploads/hospitales/${ hospital.avatar }`;
            if ( fs.existsSync(pathViejo) ) {
                fs.unlinkSync(pathViejo);
            }

            // Luego de eliminar el path viejo actualizamos la imagen del medico en la DB
            hospital.avatar = nombreArchivo;
            await hospital.save();
            return true;

            break;
        case 'usuarios':
            // Verificar si existe el medico, si no existe retornamos falso para terminar
            const usuario = await Usuario.findById(id);
            if ( !usuario ) {
                console.log('No es un usuario por id');
                return false;
            }

            // Si existe debemos saber la ruta de la vieja imagen para eliminarla
            pathViejo = `./uploads/usuarios/${ usuario.avatar }`;
            if ( fs.existsSync(pathViejo) ) {
                fs.unlinkSync(pathViejo);
            }

            // Luego de eliminar el path viejo actualizamos la imagen del medico en la DB
            usuario.avatar = nombreArchivo;
            await usuario.save();
            return true;

            break;
        default:
            break;
    }

}

module.exports = helpers;
