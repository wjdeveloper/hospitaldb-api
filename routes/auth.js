const { Router } = require('express');
const router = Router();

const { check } = require('express-validator');
const { validarCampos } = require('../helpers/index');

const { login } = require('../controllers/auth');

router.post('/login', [
    check('email', 'El email es obligatorio').isEmail(),
    check('password', 'La password es obligatoria').not().isEmpty(),
    validarCampos
], login);

module.exports = router;
