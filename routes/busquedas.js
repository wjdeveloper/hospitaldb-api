const { Router } = require('express');
const router = Router();

const { check } = require('express-validator');
const { validarCampos, validarToken } = require('../helpers');

const { todo, documentosCollection } = require('../controllers').busqueda;


router.get('/:termino', validarToken, todo);
router.get('/collection/:tabla/:termino', validarToken, documentosCollection);


module.exports = router;
