const hospitales = require('./hospitales');
const medicos = require('./medicos');
const usuarios = require('./usuarios');
const auth = require('./auth');
const busquedas = require('./busquedas');
const uploads = require('./uploads');


module.exports = app => {

    app.use('/api/v1/hospitales', hospitales);
    app.use('/api/v1/medicos', medicos);
    app.use('/api/v1/usuarios', usuarios);
    app.use('/api/v1/auth', auth);
    app.use('/api/v1/busquedas', busquedas);
    app.use('/api/v1/uploads', uploads);

}
