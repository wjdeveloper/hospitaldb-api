const { Router } = require('express');
const expressFileUpload = require('express-fileupload');
const router = Router();

const { check } = require('express-validator');
const { validarCampos, validarToken } = require('../helpers');

const { fileUpload, obtenerImagen } = require('../controllers').uploads;

router.use(expressFileUpload());

router.put('/:tipo/:id', fileUpload);
router.get('/:tipo/:image', obtenerImagen);


module.exports = router;
