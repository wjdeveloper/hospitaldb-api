const { Router } = require('express');
const router = Router();

const {
    obtenerUsuarios,
    crearUsuario,
    actualizarUsuario,
    eliminarUsuario
} = require('../controllers/usuario');

const { check } = require('express-validator');
const { validarCampos, validarToken } = require('../helpers');


// Usuarios
router.get('/', [ validarToken ], obtenerUsuarios);
router.post('/', [
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('email', 'El email no es valido').isEmail(),
    check('password', 'La password es obligatoria').not().isEmpty(),
    validarCampos
], crearUsuario);
router.put('/:id', [
    validarToken,
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('email', 'El email no es valido').isEmail(),
    check('role', 'El role es obligatorio').not().isEmpty(),
    check('id', 'El id no es un ObjectId valido').isMongoId(),
    validarCampos
], actualizarUsuario);
router.delete('/:id', [
    validarToken,
    check('id', 'El id no es un ObjectId valido').isMongoId(),
    validarCampos
], eliminarUsuario);

module.exports = router;
