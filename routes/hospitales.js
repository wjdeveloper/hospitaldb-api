const { Router } = require('express');
const router = Router();

const { check } = require('express-validator');
const { validarCampos, validarToken } = require('../helpers');

const {
    obtenerHospitales,
    crearHospital,
    actualizarHospital,
    eliminarHospital
} = require('../controllers').hospitales;

router.get('/', obtenerHospitales);
router.post('/', [
    validarToken,
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    validarCampos
], crearHospital);
router.put('/:id', actualizarHospital);
router.delete('/:id', eliminarHospital);

module.exports = router;
