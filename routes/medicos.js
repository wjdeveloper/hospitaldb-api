const { Router } = require('express');
const router = Router();

const { check } = require('express-validator');
const { validarCampos, validarToken } = require('../helpers');

const {
    obtenerMedicos,
    crearMedico,
    actualizarMedico,
    eliminarMedico
} = require('../controllers').medicos;

router.get('/', obtenerMedicos);
router.post('/:hospital', [
    validarToken,
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('hospital', 'El hospital id no es un MongoId valido').isMongoId(),
    validarCampos
], crearMedico);
router.put('/:id', actualizarMedico);
router.delete('/:id', eliminarMedico);

module.exports = router;
