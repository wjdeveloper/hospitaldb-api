const mongoose = require('mongoose');
const { database } = require('./keys');

const ctrl = {}

ctrl.connect = async () => {

    await mongoose.connect(database.uri, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false});
    console.log('DB connected succesfull');
}

module.exports = ctrl;
