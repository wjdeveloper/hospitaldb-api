const { Schema, model } = require('mongoose');

const hospitalSchema = Schema({

    nombre: {
        type: String,
        required: true
    },
    avatar: {
        type: String
    },
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true
    },
    timestamp: {
        type: Date,
        default: Date.now
    }

}, { collection: 'hospitales' });

// Esta function extrae del toObject los atributos que no deseo mostrar
hospitalSchema.method('toJSON', function() {
    const { __v, ...object } = this.toObject();
    return object;
})

module.exports = model('Hospital', hospitalSchema);
