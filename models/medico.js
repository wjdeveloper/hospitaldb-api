const { Schema, model } = require('mongoose');

const medicoSchema = Schema({

    nombre: {
        type: String,
        required: true
    },
    avatar: {
        type: String
    },
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true
    },
    hospital: {
        type: Schema.Types.ObjectId,
        ref: 'Hospital',
        required: true
    },
    timestamp: {
        type: Date,
        default: Date.now
    }

}, { collection: 'medicos' });

// Esta function extrae del toObject los atributos que no deseo mostrar
medicoSchema.method('toJSON', function() {
    const { __v, ...object } = this.toObject();
    return object;
})

module.exports = model('Medico', medicoSchema);
