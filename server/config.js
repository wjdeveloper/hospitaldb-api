const express = require('express');
const cors = require('cors');
// const expressFileUpload = require('express-fileupload');
const dotenv = require('dotenv').config();

const routes = require('../routes');
const database = require('../database');

module.exports = app => {

    // Database connection
    database.connect();

    // Directorio publico
    app.use(express.static('public'));

    app.set('port', process.env.PORT || 3000);
    app.set('env', process.env.NODE_ENV);

    app.use(cors());
    // app.use(expressFileUpload());
    app.use(express.urlencoded({extended: false}));
    app.use(express.json());


    routes(app);
    return app;
}
